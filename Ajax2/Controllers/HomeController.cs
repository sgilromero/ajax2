﻿using System.Web.Caching;
using System.Web.Mvc;

namespace Ajax2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Validation(string message)
        {
            return Json(message);
        }
    }
}